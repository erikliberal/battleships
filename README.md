# Projeto de Inteligência artificial
## Jogo de Batalha naval

### Pesquisar: ###
* Soluções de algoritmo inteligente para um jogo de batalha naval
* Agentes concorrentes

### Regras do modelo ###
* Devem existir barcos de diversos tamanhos e nomes
* Devem existir dois jogadores competidores
* Existirão dois espaços de jogo possuídos por cada um dos jogadores e o um jogador não pode estar ciente do que existe no espaço do outro jogador
* Um jogador deve poder posicionar barcos em seu espaço de jogo
* Uma rodada inicia com a declaração de jogada do jogador 1 para o jogador 2 declarando qual célula do espaço inimigo está sendo atacada, tal ação deve ter um retorno que pode ser algo entre **ACERTO**, **ÁGUA**, ou no caso de ter atingido um barco que não pode receber mais disparos o retorno deve ser o nome do barco
* Uma rodada finaliza assim que o último jogador efetuar sua ação de disparo
* A partida encerra quando todos os barcos de um jogador são afundados
* Um jogador somente pode posicionar os barcos antes da partida ter sido iniciada

[Referências](./references.md)