package agent;

import environment.io.Action;
import environment.io.Perception;

public abstract class Agent {
  private String id;

  public Agent(String id) {
    this.id = id;
  }

  @Override
  public final boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Agent other = (Agent) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    }
    else if (!id.equals(other.id))
      return false;
    return true;
  }

  public abstract Action act(Perception perception);

  public String getId() {
    return id;
  }
}
