package environment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import agent.Agent;
import environment.io.Action;
import environment.io.Perception;

public abstract class Environment {

    private List<Agent> agents = new ArrayList<Agent>();

    public void addAgent(Agent agent) {
	if (!agents.contains(agent)) {
	    agents.add(agent);
	}
    }

    public void playTurn() {
	Collections.shuffle(agents);
	for (Agent agent : agents) {
	    Perception perception = calculatePerception(agent);
	    Action action = agent.act(perception);
	    applyAction(agent, action);
	}
    }

    protected abstract void applyAction(Agent author, Action action);

    protected abstract Perception calculatePerception(Agent agent);

    protected int getAgentCount() {
	return agents.size();
    }
}
