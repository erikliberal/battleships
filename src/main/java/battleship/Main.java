package battleship;

import java.lang.reflect.InvocationTargetException;

import battleship.agent.BattleShipPlayer;
import battleship.environment.BattleField;
import battleship.match.Match;
import battleship.match.Match.MatchResult;

public class Main {

    public static BattleShipPlayer getPlayerByClassName(String className) throws ClassNotFoundException,
            InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException,
            NoSuchMethodException, SecurityException {
        Class<?> clazz = Class.forName("battleship.agent.impl." + className);
        String simpleName = clazz.getName();
        simpleName = simpleName.substring(simpleName.lastIndexOf(".") + 1, simpleName.length());
        return (BattleShipPlayer) clazz.getConstructor(String.class).newInstance(simpleName);
    }

    public static void main(final String[] args) {
        try {
            final BattleShipPlayer player1 = getPlayerByClassName(args.length > 0 ? args[0] : "JollyRoger");
            final BattleShipPlayer player2 = getPlayerByClassName(args.length > 1 ? args[1] : "RandomPlayer");

            while ((player1.getPoints() < 5 && player2.getPoints() < 5)
                    || Math.abs(player1.getPoints() - player2.getPoints()) < 2) {
                playMatch(player1, player2);
            }

            if (player1.getPoints() > player2.getPoints()) {
                System.out.println(player1.getId() + " é o vencedor! " + player1.getPoints() + "x"
                        + player2.getPoints());
            } else {
                System.out
                        .println(player2.getId() + " é o vencedor!" + player2.getPoints() + "x" + player1.getPoints());
            }
        } catch (Exception e) {
            System.out.println("ERROR LIST =======");
            e.printStackTrace();
            System.out.println("ERROR LIST =======");
        }
    }

    private static void playMatch(BattleShipPlayer player1, BattleShipPlayer player2) throws InterruptedException {
        Match match = new Match(player1, player2);

        match.start();

        BattleField.printTwoFields(match);
        System.out.println();

        while (!match.isFinished()) {
            clearScreen();
            match.playTurn();
            BattleField.printTwoFields(match);
            System.out.println();
            System.out.println();
            Thread.sleep(500);
        }

        clearScreen();
        System.out.println();
        System.out.println();
        BattleField.printTwoFields(match);
        MatchResult result = match.getResult();
        if (result.isTie()) {
            System.out.println("Jogadores empataram!");
        } else {
            System.out.println(result.getWinner().getId() + " é o vencedor da partida!");
            result.getWinner().addPoint();
        }
        Thread.sleep(1000);
    }

    private static void clearScreen() {
        for (int i = 0; i < 120; i++) {
            System.out.println();
        }
    }
}
