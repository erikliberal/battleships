package battleship.environment;

import battleship.ships.Ship;

public class FieldCell {
    private final Ship ship;
    private boolean hit = false;

    public FieldCell(final Ship ship) {
        this.ship = ship;
    }

    public boolean isHit() {
        return this.hit;
    }

    public Ship getShip() {
        return this.ship;
    }

    public void setHit() {
        this.hit = true;
    }

}