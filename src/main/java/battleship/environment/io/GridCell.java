package battleship.environment.io;

public enum GridCell {
    WATER(' '), AIRCRAFT_CARRIER('C'), BATTLE_SHIP('B'), DESTROYER('D'), SUBMARINE(
            'S'), PATROL_BOAT('P'), HIT_WATER('~'), HIT('#');

    private char xar;

    private GridCell(final char xar) {
        this.xar = xar;
    }

    public char getChar() {
        return this.xar;
    }
}