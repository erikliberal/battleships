package battleship.environment.io;

import environment.io.Perception;

public class BattleGrid implements Perception {

    private static final int GRID_HEIGHT = 10;
    private static final int GRID_WIDTH = 10;
    private GridCell[][] grid;

    public BattleGrid(GridCell[][] grid) {
        this.grid = grid;
    }

    public GridCell getCell(char col, int row) {
        return this.grid[row - 1][col - 'A'];
    }

    @Override
    public String toString() {
        String result = "    A  B  C  D  E  F  G  H  I  J \n";
        result += "\n";
        for (int i = 1; i <= GRID_HEIGHT; i++) {
            for (int sl = 0; sl < 3; sl++) {
                if (sl == 1) {
                    if (i < 10) {
                        result += " ";
                    }
                    result += i;
                    result += " ";
                } else {
                    result += "   ";
                }
                for (int j = 1; j <= GRID_WIDTH; j++) {
                    for (int sc = 0; sc < 3; sc++) {
                        result += this.grid[i - 1][j - 1].getChar();
                    }
                }
                result += "\n";
            }
        }
        return result;
    }
}