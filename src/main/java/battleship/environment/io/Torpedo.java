/**
 * 
 */
package battleship.environment.io;

import battleship.agent.BattleShipPlayer;
import environment.io.Action;

public class Torpedo implements Action {

    private final char col;
    private final int row;
    private final BattleShipPlayer player;

    public BattleShipPlayer getPlayer() {
        return this.player;
    }

    public Torpedo(BattleShipPlayer player, char col, int row) {
        this.col = col;
        this.row = row;
        this.player = player;
        System.out.println("(" + player.getPoints() + ") " + player.getId()
                + ": \t" + col + " " + row);
    }

    public int getCol() {
        return this.col;
    }

    public int getRow() {
        return this.row;
    }
}