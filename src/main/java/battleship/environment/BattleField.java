package battleship.environment;

import java.util.Random;

import agent.Agent;
import battleship.environment.io.BattleGrid;
import battleship.environment.io.GridCell;
import battleship.environment.io.Torpedo;
import battleship.match.Match;
import battleship.ships.AircraftCarrier;
import battleship.ships.BattleShip;
import battleship.ships.Destroyer;
import battleship.ships.Direction;
import battleship.ships.PatrolBoat;
import battleship.ships.Ship;
import battleship.ships.Submarine;
import environment.Environment;
import environment.io.Action;
import environment.io.Perception;

public class BattleField extends Environment {
    private static final int GRID_HEIGHT = 10;
    private static final int GRID_WIDTH = 10;
    private final FieldCell[][] field = new FieldCell[GRID_HEIGHT][GRID_WIDTH];
    private int shipsSunk = 0;

    public BattleField(final AircraftCarrier aircraftCarrier, final BattleShip battleShip, final Destroyer destroyer,
            final Submarine submarine, final PatrolBoat patrolBoat) {
        this.place(aircraftCarrier);
        this.place(battleShip);
        this.place(destroyer);
        this.place(submarine);
        this.place(patrolBoat);
        this.fillWater();
    }

    private void fillWater() {
        for (int i = 0; i < GRID_HEIGHT; i++) {
            for (int j = 0; j < GRID_WIDTH; j++) {
                if (this.field[i][j] == null) {
                    this.field[i][j] = new FieldCell(null);
                }
            }
        }
    }

    private void place(final Ship ship) {
        int col = (ship.getCol() - 'A');
        int row = ship.getRow() - 1;
        final int colinc = ship.getDirection() == Direction.HORIZONTAL ? 1 : 0;
        final int rowinc = ship.getDirection() == Direction.VERTICAL ? 1 : 0;

        for (int i = 0; i < ship.getSize(); i++, col += colinc, row += rowinc) {
            if (this.field[row][col] == null) {
                this.field[row][col] = new FieldCell(ship);
            } else {
                throw new RuntimeException("Ships collision");
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see environment.Environment#applyAction(agent.Agent,
     * environment.io.Action)
     */
    @Override
    protected void applyAction(final Agent author, final Action action) {
        final Torpedo t = (Torpedo) action;
        final int col = (t.getCol() - 'A');
        final int row = t.getRow() - 1;
        final FieldCell fieldCell = this.field[row][col];
        if (!fieldCell.isHit()) {
            final Ship ship = fieldCell.getShip();
            if (ship != null) {
                ship.hit();
                if (ship.isSunk()) {
                    this.shipsSunk++;
                }
            }
            fieldCell.setHit();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see environment.Environment#calculatePerception(agent.Agent)
     */
    @Override
    protected Perception calculatePerception(final Agent agent) {
        GridCell grid[][] = new GridCell[GRID_HEIGHT][GRID_WIDTH];
        for (int i = 0; i < GRID_HEIGHT; i++) {
            for (int j = 0; j < GRID_WIDTH; j++) {
                if (!this.field[i][j].isHit()) {
                    this.setCell(grid, i, j, GridCell.WATER);
                } else {
                    final Ship ship = this.field[i][j].getShip();
                    if (ship == null) {
                        this.setCell(grid, i, j, GridCell.HIT_WATER);
                    } else {
                        if (ship.isSunk()) {
                            this.setCell(grid, i, j, ship.getCell());
                        } else {
                            this.setCell(grid, i, j, GridCell.HIT);
                        }
                    }
                }
            }
        }
        final BattleGrid battleGrid = new BattleGrid(grid);
        return battleGrid;
    }

    private void setCell(GridCell grid[][], final int i, final int j, final GridCell cell) {
        grid[i][j] = cell;
    }

    public void printField() {
        System.out.println(this.calculatePerception(null));
    }

    public int getShipsSunk() {
        return this.shipsSunk;
    }

    public static void printTwoFields(final BattleField battleField, final BattleField battleField2) {
        String print = "    A  B  C  D  E  F  G  H  I  J      A  B  C  D  E  F  G  H  I  J \n";
        print += "\n";
        final BattleGrid grid = (BattleGrid) battleField.calculatePerception(null);
        final BattleGrid grid2 = (BattleGrid) battleField2.calculatePerception(null);
        for (int i = 1; i <= GRID_HEIGHT; i++) {
            for (int sl = 0; sl < 3; sl++) {
                if (sl == 1) {
                    if (i < 10) {
                        print += " ";
                    }
                    print += i;
                    print += " ";
                } else {
                    print += "   ";
                }
                for (int j = 1; j <= GRID_WIDTH; j++) {
                    for (int sc = 0; sc < 3; sc++) {
                        print += grid.getCell((char) (j - 1 + 'A'), i).getChar();
                    }
                }
                print += " ";

                // O outro grid
                if (sl == 1) {
                    if (i < 10) {
                        print += " ";
                    }
                    print += i;
                    print += " ";
                } else {
                    print += "   ";
                }
                for (int j = 1; j <= GRID_WIDTH; j++) {
                    for (int sc = 0; sc < 3; sc++) {
                        print += grid2.getCell((char) (j - 1 + 'A'), i).getChar();
                    }
                }
                print += " ";

                // A coluna final
                if (sl == 1) {
                    if (i < 10) {
                        print += " ";
                    }
                    print += i;
                }

                print += "\n";
            }
        }
        print += "\n    A  B  C  D  E  F  G  H  I  J       A  B  C  D  E  F  G  H  I  J ";
        System.out.println(print);
    }

    public static BattleField randomBattleField() {
        while (true) {
            try {
                final Random r = new Random();
                int row = r.nextInt(10);
                int col = r.nextInt(10);
                Direction direction = r.nextInt(2) == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;
                final AircraftCarrier ac = new AircraftCarrier((char) (col + 'A'), row + 1, direction);

                row = r.nextInt(10);
                col = r.nextInt(10);
                direction = r.nextInt(2) == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;
                final BattleShip bs = new BattleShip((char) (col + 'A'), row + 1, direction);

                row = r.nextInt(10);
                col = r.nextInt(10);
                direction = r.nextInt(2) == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;
                final Destroyer ds = new Destroyer((char) (col + 'A'), row + 1, direction);

                row = r.nextInt(10);
                col = r.nextInt(10);
                direction = r.nextInt(2) == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;
                final Submarine sb = new Submarine((char) (col + 'A'), row + 1, direction);

                row = r.nextInt(10);
                col = r.nextInt(10);
                direction = r.nextInt(2) == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;
                final PatrolBoat pb = new PatrolBoat((char) (col + 'A'), row + 1, direction);
                return new BattleField(ac, bs, ds, sb, pb);
            } catch (final Exception e) {

            }
        }
    }

    public static void printTwoFields(Match match) {
        printTwoFields(match.getBattleField(), match.getBattleField2());
    }
}
