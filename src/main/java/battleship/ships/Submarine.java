package battleship.ships;

import battleship.environment.io.GridCell;

public class Submarine extends Ship {
    public Submarine(final char col, final int row, final Direction direction) {
        this.col = col;
        this.row = row;
        this.direction = direction;
        this.size = 3;
        this.cell = GridCell.SUBMARINE;
    }
}