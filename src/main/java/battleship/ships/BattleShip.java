package battleship.ships;

import battleship.environment.io.GridCell;

public class BattleShip extends Ship {
    public BattleShip(final char col, final int row, final Direction direction) {
        this.col = col;
        this.row = row;
        this.direction = direction;
        this.size = 4;
        this.cell = GridCell.BATTLE_SHIP;
    }
}