package battleship.ships;

import battleship.environment.io.GridCell;

public abstract class Ship {

    protected char col;
    protected int row;
    protected Direction direction;
    protected int size;
    protected GridCell cell;
    protected int hitCount = 0;

    public char getCol() {
        return this.col;
    }

    public int getRow() {
        return this.row;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public int getSize() {
        return this.size;
    }

    public GridCell getCell() {
        return this.cell;
    }

    public void hit() {
        this.hitCount++;
    }

    public boolean isSunk() {
        return this.hitCount == this.size;
    }
}