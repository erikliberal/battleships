package battleship.ships;

import battleship.environment.io.GridCell;

public class AircraftCarrier extends Ship {
    public AircraftCarrier(final char col, final int row,
            final Direction direction) {
        this.col = col;
        this.row = row;
        this.direction = direction;
        this.size = 5;
        this.cell = GridCell.AIRCRAFT_CARRIER;
    }
}