package battleship.ships;

import battleship.environment.io.GridCell;

public class PatrolBoat extends Ship {
    public PatrolBoat(final char col, final int row, final Direction direction) {
        this.col = col;
        this.row = row;
        this.direction = direction;
        this.size = 2;
        this.cell = GridCell.PATROL_BOAT;
    }
}