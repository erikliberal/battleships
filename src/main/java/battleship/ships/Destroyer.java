package battleship.ships;

import battleship.environment.io.GridCell;

public class Destroyer extends Ship {
    public Destroyer(final char col, final int row, final Direction direction) {
        this.col = col;
        this.row = row;
        this.direction = direction;
        this.size = 3;
        this.cell = GridCell.DESTROYER;
    }
}