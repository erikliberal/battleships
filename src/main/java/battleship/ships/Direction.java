package battleship.ships;

public enum Direction {
    VERTICAL, HORIZONTAL
}
