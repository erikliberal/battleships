package battleship.game;

public class ShipInstance {
    private int hits;
    private final Ship ship;

    public ShipInstance(Ship ship) {
        this.ship = ship;
        this.hits = 0;
    }

    public Shot hit() {
        return new Shot(true, ++hits >= ship.getSize() ? ship : null);
    }

}
