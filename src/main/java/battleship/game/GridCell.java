package battleship.game;

public class GridCell {

    private boolean shotAt;
    private boolean hit;
    private Ship ship;

    public GridCell() {
        this.shotAt = false;
        this.hit = false;
    }

    public boolean isShotAt() {
        return this.shotAt;
    }

    public boolean isHit() {
        return this.hit;
    }

    public Ship getShip() {
        return ship;
    }

    public void updateCell(Ship ship) {
        this.ship = ship;
    }

    public void hit(Shot shot) {
        this.shotAt = true;
        this.hit = shot != null && shot.isHit();
        this.ship = shot.getShip();
    }

    @Override
    public String toString() {
        String result = " ";
        if (ship == null) {
            if (shotAt) {
                if (hit) {
                    result = "#";
                } else {
                    result = "~";
                }
            }
        } else {
            result = ship.toString();
        }
        return result;
    }

}
