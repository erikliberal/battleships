package battleship.game;

public abstract class BattleShipsAI extends Player implements BattleShipsAgent {
    public BattleShipsAI(String id) {
        super(id);
    }

    public void updatePersonalView(Ship ship, Position position, Direction direction) {
        int col = position.getColumn();
        int row = position.getRow();
        if (Direction.HORIZONTAL.equals(direction)) {
            for (int i = 0; i < ship.getSize(); i++) {
                getPlayerGrid().getCell(new Position(col + i, row)).updateCell(ship);
            }
        } else if (Direction.VERTICAL.equals(direction)) {
            for (int i = 0; i < ship.getSize(); i++) {
                getPlayerGrid().getCell(new Position(col, row + i)).updateCell(ship);
            }
        }
    }

}
