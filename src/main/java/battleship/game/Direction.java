package battleship.game;

public enum Direction {
    VERTICAL, HORIZONTAL
}
