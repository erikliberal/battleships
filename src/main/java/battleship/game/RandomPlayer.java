/**
 * 
 */
package battleship.game;

import java.util.Map.Entry;
import java.util.Random;

public class RandomPlayer extends BattleShipsAI {

    public RandomPlayer(String id) {
        super(id);
    }

    @Override
    public void placeShips() {
        for (Entry<Ship, Integer> entry : getCurrentMatch().getShipPlacementRequirements()) {
            for (int i = 0; i < entry.getValue(); i++) {
                Ship ship = entry.getKey();
                final Random r = new Random();
                Position position;
                Direction direction;
                do {
                    position = new Position(r.nextInt(10) + 1, r.nextInt(10) + 1);
                    direction = r.nextInt(2) == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;

                } while (!getCurrentMatch().isPlacementAllowed(this, ship, position, direction));
                getCurrentMatch().placeShip(this, ship, position, direction);
                updatePersonalView(ship, position, direction);
            }
        }
    }

    @Override
    public void shoot() {
        if (!this.equals(getCurrentMatch().getCurrentPlayer())) {
            return;
        }
        BattleGrid grid = calculatePerception();
        Position position;
        GridCell gridCell;
        do {
            final Random r = new Random();
            position = new Position((char) (r.nextInt(10) + 'A'), r.nextInt(10) + 1);
        } while ((gridCell = grid.getCell(position)).isShotAt());
        Shot shot = getCurrentMatch().shoot(this, position);
        gridCell.hit(shot);
    }
}
