package battleship.game;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Score {
    private int playCount;
    private final Map<Ship, Row> scoreRows;

    public Score(Set<Entry<Ship, Integer>> maxShips) {
        this.playCount = 0;
        this.scoreRows = new HashMap<>();
        for (Entry<Ship, Integer> entry : maxShips) {
            this.scoreRows.put(entry.getKey(), new Row(entry.getValue()));
        }
    }

    public int getPlayCount() {
        return this.playCount;
    }

    public void sink(Ship ship) {
        this.scoreRows.get(ship).increment();
    }

    public int getAmmount(Ship ship) {
        return this.scoreRows.get(ship).maxAmmount;
    }

    public int getAmmountSunk(Ship ship) {
        return this.scoreRows.get(ship).sunk;
    }

    public Set<Entry<Ship, Row>> getCurrent() {
        return scoreRows.entrySet();
    }

    public static final class Row {
        private final int maxAmmount;
        private int sunk;

        private Row(int maxAmmount) {
            this.maxAmmount = maxAmmount;
            this.sunk = 0;
        }

        private void increment() {
            if (this.sunk == maxAmmount) {
                throw new IllegalStateException("Cannot sink more ships than the ammount available");
            }
            this.sunk++;
        }

        public int getMaxAmmount() {
            return maxAmmount;
        }

        public int getSunk() {
            return sunk;
        }

    }

    public void incrementPlayCount() {
        this.playCount++;
    }
}
