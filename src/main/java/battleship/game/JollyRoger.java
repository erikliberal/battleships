package battleship.game;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

/**
 * @author Erik Liberal(erik.liberal@gmail.com)
 */
public class JollyRoger extends BattleShipsAI {
    private Map<Position, CellContainer> cells;
    private Set<Ship> sunkShips;
    private CellContainer currentChoice;

    private void addWeightCell(int col, int row, int weight) {
        if ((col < 1) || (col > 10) || (row < 1) || (row > 10)) {
            return;
        }
        CellContainer aux = cells.get(new Position(col, row));
        aux.weight += weight;
    }

    /* Os vizinhos relevantes ser�o sempre os verticais e horizontais */
    private void addWeightNeighbors(int col, int row, int weight) {
        addWeightCell(col - 1, row, weight);
        addWeightCell(col + 1, row, weight);
        addWeightCell(col, row - 1, weight);
        addWeightCell(col, row + 1, weight);
    }

    private void calculateChances() {
        for (int row = 1; row < 11; row++) {
            for (int col = 1; col < 11; col++) {
                CellContainer aux = cells.get(new Position(col, row));
                aux.weight = 0;

                GridCell gridCell = getOpponentGrid().getCell(new Position(col, row));
                aux.state = gridCell;

                if (aux.state.getShip() != null) {
                    sunkShips.add(aux.state.getShip());
                }

            }
        }
        for (CellContainer c : cells.values()) {
            if (!c.state.isShotAt()) {
                addWeightNeighbors(c.position.getColumn(), c.position.getRow(), 1);
                if (!sunkShips.contains(Ship.AIRCRAFT_CARRIER)) {
                    c.weight += getContinuousAmmount(c.position.getColumn(), c.position.getRow(), 5);
                }
                if (!sunkShips.contains(Ship.BATTLESHIP)) {
                    c.weight += getContinuousAmmount(c.position.getColumn(), c.position.getRow(), 4);
                }
                if (!sunkShips.contains(Ship.DESTROYER)) {
                    c.weight += getContinuousAmmount(c.position.getColumn(), c.position.getRow(), 3);
                }
                if (!sunkShips.contains(Ship.SUBMARINE)) {
                    c.weight += getContinuousAmmount(c.position.getColumn(), c.position.getRow(), 3);
                }
                if (!sunkShips.contains(Ship.PATROL_BOAT)) {
                    c.weight += getContinuousAmmount(c.position.getColumn(), c.position.getRow(), 2);
                }
            } else if (c.state.isHit()) {
                finishIt(c.position.getColumn(), c.position.getRow());
            }
        }
    }

    private void finishIt(int col, int row) {
        CellContainer aux = cells.get(new Position(col, row - 1));
        if (aux != null && aux.state.isHit()) {
            addWeightCell(col, row + 1, 60);
        } else {
            addWeightCell(col, row + 1, 30);
        }
        aux = cells.get(new Position(col, row + 1));
        if (aux != null && aux.state.isHit()) {
            addWeightCell(col, row - 1, 60);
        } else {
            addWeightCell(col, row - 1, 30);
        }
        aux = cells.get(new Position(col - 1, row));
        if (aux != null && aux.state.isHit()) {
            addWeightCell((char) (col + 1), row, 60);
        } else {
            addWeightCell((char) (col + 1), row, 30);
        }
        aux = cells.get(new Position(col + 1, row));
        if (aux != null && aux.state.isHit()) {
            addWeightCell((char) (col - 1), row, 60);
        } else {
            addWeightCell((char) (col - 1), row, 30);
        }
    }

    private int getContinuousAmmount(int col, int row, int jumps) {
        int result = 0;
        for (int i = 0; i < 4; i++) {
            if (hasContinuity(col, row, i, jumps)) {
                result++;
            }
        }
        return result;
    }

    private boolean hasContinuity(int col, int row, int direction, int jumps) {
        // Directions: 0 - up, 1 - right, 2 - down, 3 - left
        for (int i = 0; i < jumps; i++) {
            if ((col < 1) || (col > 10) || (row < 1) || (row > 10)
                    || cells.get(new Position(col, row)).state.isShotAt()) {
                return false;
            }
            switch (direction) {
            case 0:
                row--;
                break;
            case 1:
                col++;
                break;
            case 2:
                row++;
                break;
            case 3:
                col--;
                break;
            }
        }

        return true;
    }

    private CellContainer getHighestProb() {
        currentChoice = new CellContainer(Position.NULL, Integer.MIN_VALUE);
        for (CellContainer o : cells.values()) {
            if (!o.state.isShotAt() && (o.weight > currentChoice.weight)) {
                currentChoice = o;
            }
        }
        if (currentChoice == null) {
            currentChoice = getRandom();
        }
        return currentChoice;
    }

    private CellContainer getRandom() {
        int row = 0;
        int col = 0;

        do {
            final Random r = new Random();
            row = r.nextInt(10);
            col = r.nextInt(10);
        } while (cells.get(new Position(col, row)).state.isShotAt());
        return cells.get(new Position(col, row));
    }

    public JollyRoger(final String id) {
        super(id);
        this.cells = new HashMap<>();
        this.sunkShips = new HashSet<Ship>();

        CellContainer cur;
        for (int i = 0; i < 10; i++) {
            for (int j = 1; j < 11; j++) {
                Position position = new Position((char) ('A' + i), j);
                cur = new CellContainer(position);
                cells.put(position, cur);
            }
        }
    }

    private static class CellContainer implements Comparable<CellContainer> {
        private GridCell state;
        private int weight;
        private Position position;

        public CellContainer(Position position, int weight) {
            this.position = position;
            this.weight = weight;
            this.state = new GridCell();
        }

        public CellContainer(Position position) {
            this.position = position;
            this.weight = 0;
            this.state = new GridCell();
        }

        @Override
        public String toString() {
            return "[" + position + "(" + weight + ")" + "]" + this.state;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + position.getColumn();
            result = prime * result + position.getRow();
            result = prime * result + ((state == null) ? 0 : state.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            CellContainer other = (CellContainer) obj;
            if (position.getColumn() != other.position.getColumn()) {
                return false;
            }
            if (position.getRow() != other.position.getRow()) {
                return false;
            }
            if (state == null) {
                if (other.state != null) {
                    return false;
                }
            } else if (!state.equals(other.state)) {
                return false;
            }
            return true;
        }

        @Override
        public int compareTo(CellContainer arg0) {
            if (this.equals(arg0)) {
                return 0;
            }
            int result = this.weight - arg0.weight;
            if (result == 0) {
                result = this.hashCode() - arg0.hashCode();
            }
            return result;
        }

    }

    @Override
    public void placeShips() {
        for (Entry<Ship, Integer> entry : getCurrentMatch().getShipPlacementRequirements()) {
            for (int i = 0; i < entry.getValue(); i++) {
                Ship ship = entry.getKey();
                final Random r = new Random();
                Position position;
                Direction direction;
                do {
                    position = new Position(r.nextInt(10) + 1, r.nextInt(10) + 1);
                    direction = r.nextInt(2) == 0 ? Direction.HORIZONTAL : Direction.VERTICAL;

                } while (!getCurrentMatch().isPlacementAllowed(this, ship, position, direction));
                getCurrentMatch().placeShip(this, ship, position, direction);
                updatePersonalView(ship, position, direction);
            }
        }
        calculateChances();
        getHighestProb();
    }

    @Override
    public void shoot() {
        if (!this.equals(getCurrentMatch().getCurrentPlayer())) {
            return;
        }
        BattleGrid grid = calculatePerception();
        GridCell gridCell = grid.getCell(currentChoice.position);
        Shot shot = getCurrentMatch().shoot(this, currentChoice.position);
        gridCell.hit(shot);
        calculateChances();
        getHighestProb();
    }

}
