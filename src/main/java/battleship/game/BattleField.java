package battleship.game;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import battleship.game.Score.Row;

public class BattleField {
    private final Player player;
    private final Score score;
    private final Grid grid;

    public BattleField(Player player, int gridSize, Set<Entry<Ship, Integer>> maxShips) {
        this.player = player;
        this.grid = new Grid(gridSize);
        this.score = new Score(maxShips);
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isPlacementAllowed(Ship ship, Position position, Direction direction) {
        return grid.isPlacementAllowed(ship, position, direction);
    }

    public Shot shoot(BattleField target, Position position) {
        Shot shot = target.grid.shoot(position);
        Ship ship = shot.getShip();
        if (shot.isHit() && ship != null) {
            score.sink(ship);
        }
        score.incrementPlayCount();
        return shot;
    }

    public void sink(Ship ship) {
        score.sink(ship);
    }

    public Set<Entry<Ship, Row>> getScore() {
        return this.score.getCurrent();
    }

    public void placeShip(Ship ship, Position position, Direction direction) {
        grid.placeShip(ship, position, direction);
    }

    public boolean allShipsPlaced() {
        boolean placementRequired = false;
        for (Iterator<Entry<Ship, Row>> iterator = getScore().iterator(); !placementRequired && iterator.hasNext();) {
            Entry<Ship, Row> entry = iterator.next();
            placementRequired = placementRequired
                    || entry.getValue().getMaxAmmount() > grid.getShipAmmount(entry.getKey());
        }
        return !placementRequired;
    }

}
