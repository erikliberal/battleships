package battleship.game;

public enum Ship {
    AIRCRAFT_CARRIER(5, 'A'), DESTROYER(4, 'D'), BATTLESHIP(3, 'B'), PATROL_BOAT(2, 'P'), SUBMARINE(3, 'S');

    public static final int MAX_SIZE = 5;

    private final int size;
    private char symbol;

    private Ship(int size, char symbol) {
        this.size = size;
        this.symbol = symbol;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return String.valueOf(symbol);
    }

    public char getSymbol() {
        return symbol;
    }

}
