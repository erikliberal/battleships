package battleship.game;

public interface BattleShipsPlayer {

    boolean isReady();

    void setReady(boolean ready);

}
