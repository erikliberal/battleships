package battleship.game;

public class Torpedo implements Action {
    private final Player player;
    private final Position position;

    public Torpedo(Player player, Position position) {
        this.player = player;
        this.position = position;
    }

    public Torpedo(Player player, char column, int row) {
        this.player = player;
        this.position = new Position(column, row);
    }

    public Player getPlayer() {
        return player;
    }

    public Position getPosition() {
        return position;
    }

}
