package battleship.game;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

public class Grid {

    private final Map<Position, ShipInstance> placement;
    private final Map<Ship, Integer> shipAmmount;

    private final int gridSize;

    public Grid(int gridSize) {
        this.placement = new HashMap<>();
        this.shipAmmount = new HashMap<>();
        Ship[] values = Ship.values();
        for (int i = 0; i < values.length; i++) {
            Ship ship = Ship.values()[i];
            shipAmmount.put(ship, 0);
        }
        this.gridSize = gridSize;
    }

    public void placeShip(Ship ship, Position position, Direction direction) {
        if (!isPlacementAllowed(ship, position, direction)) {
            throw new IllegalStateException(MessageFormat.format("Positioning {0} on {1} {2} is not allowed", ship,
                    position, direction));
        }
        if (!Direction.HORIZONTAL.equals(direction) && !Direction.VERTICAL.equals(direction)) {
            throw new IllegalArgumentException("Direction should be either HORIZONTAL or VERTICAL");
        }
        ShipInstance shipInstance = new ShipInstance(ship);
        int shipSize = ship.getSize();
        if (Direction.HORIZONTAL.equals(direction)) {
            for (int i = 0; i < shipSize; i++) {
                placement.put(new Position(position.getColumn() + i, position.getRow()), shipInstance);
            }
        } else if (Direction.VERTICAL.equals(direction)) {
            for (int i = 0; i < shipSize; i++) {
                placement.put(new Position(position.getColumn(), position.getRow() + i), shipInstance);
            }
        }
        shipAmmount.put(ship, shipAmmount.get(ship) + 1);
    }

    public int getShipAmmount(Ship ship) {
        return shipAmmount.get(ship);
    }

    public boolean hasPlacedShip(Position position) {
        return placement.get(position) != null;
    }

    private boolean hasHorizontalCollision(int col, int row, int shipSize) {
        boolean hasPlacedShip = false;
        for (int i = 0; i < shipSize && !hasPlacedShip; i++) {
            hasPlacedShip = hasPlacedShip || hasPlacedShip(new Position(col + i, row));
        }
        return hasPlacedShip;
    }

    private boolean hasVerticalCollision(int col, int row, int shipSize) {
        boolean hasPlacedShip = false;
        for (int i = 0; i < shipSize && !hasPlacedShip; i++) {
            hasPlacedShip = hasPlacedShip || hasPlacedShip(new Position(col, row + i));
        }
        return hasPlacedShip;
    }

    private boolean isPlacementAllowed(Ship ship, int col, int row, Direction direction) {
        boolean result = true;
        int shipSize = ship.getSize();
        result = result && (col >= 1 && col <= gridSize);
        result = result && (row >= 1 && row <= gridSize);

        if (Direction.HORIZONTAL.equals(direction)) {
            int colEnd = col + shipSize - 1;
            result = result && (colEnd >= 1) && (colEnd <= gridSize);
            result = result && !hasHorizontalCollision(col, row, shipSize);
        } else if (Direction.VERTICAL.equals(direction)) {
            int rowEnd = row + shipSize - 1;
            result = result && (rowEnd >= 1) && (rowEnd <= gridSize);
            result = result && !hasVerticalCollision(col, row, shipSize);
        } else {
            result = false;
        }
        return result;
    }

    public boolean isPlacementAllowed(Ship ship, Position position, Direction direction) {
        int col = position.getColumn();
        int row = position.getRow();
        return isPlacementAllowed(ship, col, row, direction);
    }

    public Shot shoot(Position position) {
        final ShipInstance ship = placement.get(position);
        Shot shot = ship != null ? ship.hit() : Shot.MISS;
        return shot;
    }
}
