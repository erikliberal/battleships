package battleship.game;

import static java.text.MessageFormat.format;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import battleship.game.Score.Row;

public class Match implements Environment {

    private static final int GRID_SIZE = 10;

    private static Set<Entry<Ship, Integer>> initializeBaseMaxShips() {
        Map<Ship, Integer> map = new HashMap<>();
        Ship[] ships = Ship.values();
        for (int i = 0; i < ships.length; i++) {
            Ship ship = Ship.values()[i];
            map.put(ship, 1);
        }
        return map.entrySet();
    }

    public static enum MatchPhase {
        LOBBY, PLACEMENT, GAME, ENDED
    }

    private MatchPhase phase;
    private final BattleField[] battleFields;
    private final Set<Entry<Ship, Integer>> shipPlacementRequirements;
    private int currentPlayer;
    private Player winner;
    private int gridSize;

    public Match() {
        this(initializeBaseMaxShips(), GRID_SIZE);
    }

    public Match(Set<Entry<Ship, Integer>> shipPlacementRequirements, int gridSize) {
        this.phase = MatchPhase.LOBBY;
        this.battleFields = new BattleField[2];
        this.shipPlacementRequirements = shipPlacementRequirements;
        // this.battleFields[0] = new BattleField(player1, gridSize,
        // this.shipPlacementRequirements);
        // this.battleFields[1] = new BattleField(player2, gridSize,
        // this.shipPlacementRequirements);
        this.winner = null;
        this.gridSize = gridSize;
    }

    private void startIfAI(Player player) {
        if (player instanceof BattleShipsAI) {
            PlayerAI playerAI = new PlayerAI((BattleShipsAI) player);
            new Thread(playerAI).start();
        }
    }

    public MatchPhase getCurrentPhase() {
        return this.phase;
    }

    public boolean isPlacementAllowed(Player player, Ship ship, Position position, Direction direction) {
        return MatchPhase.PLACEMENT.equals(phase)
                && getBattleField(player).isPlacementAllowed(ship, position, direction);
    }

    private void updateCurrentPlayer() {
        currentPlayer = getNextPlayerIndex();
    }

    private int getNextPlayerIndex() {
        return (currentPlayer + 1) % 2;
    }

    public Player getCurrentPlayer() {
        return battleFields[currentPlayer].getPlayer();
    }

    public Shot shoot(Player shooter, Position position) {
        Shot shot = null;
        if (MatchPhase.GAME.equals(phase) && shooter != null && shooter.equals(getCurrentPlayer()) && !isGameEnded()) {
            shot = battleFields[currentPlayer].shoot(battleFields[getNextPlayerIndex()], position);
            updateCurrentPlayer();
        }
        return shot;
    }

    public boolean isGameEnded() {
        boolean gameEnded = winner != null;
        for (int i = 0; i < battleFields.length && !gameEnded && hasAllPlayers(); i++) {
            BattleField battleField = battleFields[i];
            boolean allSunk = true;
            for (Iterator<Entry<Ship, Row>> iterator = battleField.getScore().iterator(); iterator.hasNext() && allSunk;) {
                Entry<Ship, Row> entry = iterator.next();
                int maxAmmount = entry.getValue().getMaxAmmount();
                int sunk = entry.getValue().getSunk();
                allSunk = allSunk && maxAmmount <= sunk;
            }
            if (gameEnded = gameEnded || allSunk) {
                winner = battleField.getPlayer();
                phase = MatchPhase.ENDED;
            }
        }
        return gameEnded;
    }

    private void changePhase() {
        switch (phase) {
        case LOBBY:
            startPlacementPhase();
            break;
        case PLACEMENT:
            startGamePhase();
            break;
        default:
            break;
        }
    }

    private void startPlacementPhase() {
        if (hasAllPlayers()) {
            this.phase = MatchPhase.PLACEMENT;
        }
    }

    private boolean hasAllPlayers() {
        boolean playersReady = true;
        for (BattleField battleField : battleFields) {
            playersReady = playersReady && battleField != null;
        }
        return playersReady;
    }

    private void startGamePhase() {
        boolean playersReady = true;
        for (BattleField battleField : battleFields) {
            playersReady = playersReady && battleField.allShipsPlaced();
        }
        if (playersReady) {
            this.phase = MatchPhase.GAME;
            this.currentPlayer = 0;
        }
    }

    private BattleField getBattleField(Player player) {
        int playerPosition = getPlayerPosition(player);
        return battleFields[playerPosition];
    }

    private int getPlayerPosition(Player player) {
        int result = -1;
        for (int i = 0; player != null && i < battleFields.length; i++) {
            if (player.equals(battleFields[i].getPlayer())) {
                result = i;
                break;
            }
        }
        return result;
    }

    public void placeShip(Player player, Ship ship, Position position, Direction direction) {
        if (isPlacementAllowed(player, ship, position, direction)) {
            BattleField battleField = getBattleField(player);
            if (!battleField.allShipsPlaced()) {
                battleField.placeShip(ship, position, direction);
            }
        }
        changePhase();
    }

    public Player getWinner() {
        return this.winner;
    }

    public Shot shoot(Torpedo torpedo) {
        return shoot(torpedo.getPlayer(), torpedo.getPosition());
    }

    public Collection<Entry<Ship, Integer>> getShipPlacementRequirements() {
        return this.shipPlacementRequirements;
    }

    public int getGridWidth() {
        return gridSize;
    }

    public int getGridHeight() {
        return gridSize;
    }

    private static void printGrid(BattleShipsAI player, PrintStream out) {
        final String FIELD = " {0} ";
        BattleGrid opponentGrid = player.getOpponentGrid();
        BattleGrid playerGrid = player.getPlayerGrid();
        int width = opponentGrid.getWidth();
        int height = opponentGrid.getHeight();
        StringBuilder sb = new StringBuilder();
        sb.append(player);
        sb.append("\n");
        sb.append("   ");
        for (int j = 0; j < width; j++) {
            sb.append(format(FIELD, (char) ('A' + j)));
        }
        sb.append("   ");
        for (int j = 0; j < width; j++) {
            sb.append(format(FIELD, (char) ('A' + j)));
        }
        sb.append("\n\n");
        for (int i = 0; i < height; i++) {
            sb.append(String.format("%3d", i + 1));
            for (int j = 0; j < width; j++) {
                sb.append(format(FIELD, opponentGrid.getCell(new Position(i + 1, j + 1))));
            }
            sb.append("");
            sb.append(String.format("%3d", i + 1));
            sb.append("");
            for (int j = 0; j < width; j++) {
                sb.append(format(FIELD, playerGrid.getCell(new Position(i + 1, j + 1))));
            }
            sb.append("\n\n");
        }
        out.print(sb);
    }

    public static void main(String[] args) throws InterruptedException {
        Match match = new Match();
        BattleShipsAI player1 = new JollyRoger("Monte Carlo 1");
        BattleShipsAI player2 = new RandomPlayer("Random 2");
        player1.joinMatch(match);
        player2.joinMatch(match);
        while (Thread.activeCount() > 1) {
            Thread.sleep(1000);
            printGrid(player1, System.out);
            printGrid(player2, System.out);
        }
    }

    private static final class PlayerAI implements Runnable {

        private BattleShipsAI agent;

        private PlayerAI(BattleShipsAI agent) {
            this.agent = agent;
        }

        @Override
        public void run() {
            Match match = agent.getCurrentMatch();
            while (!match.isGameEnded()) {
                switch (match.getCurrentPhase()) {
                case PLACEMENT:
                    agent.placeShips();
                    break;
                case GAME:
                    agent.shoot();
                default:
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    public synchronized void allocate(Player player) {
        this.battleFields[currentPlayer] = new BattleField(player, gridSize, shipPlacementRequirements);
        changePhase();
        startIfAI(player);
        updateCurrentPlayer();
    }

}
