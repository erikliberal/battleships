package battleship.game;

import java.text.MessageFormat;

public class Position {
    public static final Position NULL = null;
    private final int row;
    private final int column;

    public Position(int column, int row) {
        this.column = column - 1;
        this.row = row - 1;
    }

    public Position(char column, int row) {
        this(column - 'A' + 1, row);
    }

    public int getColumn() {
        return this.column + 1;
    }

    public int getRow() {
        return this.row + 1;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + column;
        result = prime * result + row;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Position)) {
            return false;
        }
        Position other = (Position) obj;
        if (column != other.column) {
            return false;
        }
        if (row != other.row) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0}{1}", (char) ('A' + getColumn() - 1), getRow());
    }

}
