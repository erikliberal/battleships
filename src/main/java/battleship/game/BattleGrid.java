package battleship.game;

public class BattleGrid {

    private GridCell grid[][];
    private int height;
    private int width;

    public BattleGrid(int height, int width) {
        this.height = height;
        this.width = width;
        this.grid = new GridCell[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                setCell(new Position((char) ('A' + i), j + 1), new GridCell());
            }
        }
    }

    public void setCell(Position position, GridCell cell) {
        this.grid[position.getColumn() - 1][position.getRow() - 1] = cell;
    }

    public GridCell getCell(Position position) {
        return this.grid[position.getColumn() - 1][position.getRow() - 1];
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

}
