package battleship.game;

public interface BattleShipsAgent {

    BattleGrid calculatePerception();

    void placeShips();

    void shoot();

    Match getCurrentMatch();

}
