package battleship.game;

public abstract class Player {

    private String id;
    private BattleGrid playerGrid;
    private BattleGrid opponentGrid;
    private Match match;

    public Player(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Player)) {
            return false;
        }
        Player other = (Player) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public void joinMatch(Match match) {
        (this.match = match).allocate(this);
        int height = match.getGridHeight();
        int width = match.getGridWidth();
        this.opponentGrid = new BattleGrid(height, width);
        this.playerGrid = new BattleGrid(height, width);
    }

    public BattleGrid getOpponentGrid() {
        return opponentGrid;
    }

    public BattleGrid getPlayerGrid() {
        return playerGrid;
    }

    public Match getCurrentMatch() {
        return match;
    }

    public BattleGrid calculatePerception() {
        return this.opponentGrid;
    }

    @Override
    public String toString() {
        return id;
    }

}
