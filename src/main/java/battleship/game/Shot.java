package battleship.game;

public class Shot {
    public static final Shot MISS = new Shot();
    private final boolean hit;
    private final Ship ship;

    public Shot(boolean hit, Ship ship) {
        this.hit = hit;
        this.ship = ship;
    }

    public Shot(boolean hit) {
        this.ship = null;
        this.hit = hit;
    }

    public Shot() {
        this.ship = null;
        this.hit = false;
    }

    public boolean isHit() {
        return hit;
    }

    public Ship getShip() {
        return ship;
    }

}
