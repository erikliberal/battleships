package battleship.agent.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import battleship.agent.BattleShipPlayer;
import battleship.environment.io.BattleGrid;
import battleship.environment.io.GridCell;
import battleship.environment.io.Torpedo;

/**
 * @author Erik Liberal(erik.liberal@gmail.com)
 */
public class JollyRoger extends BattleShipPlayer {
    private HashMap<Integer, CellContainer> cells;
    private HashSet<GridCell> sunkShips;
    private CellContainer currentChoice;

    private void addWeightCell(char col, int row, int weight) {
        if ((col < 'A') || (col > 'J') || (row < 1) || (row > 10))
            return;
        int key = (col * 10) + row;
        CellContainer aux = cells.get(key);
        aux.weight += weight;
    }

    /* Os vizinhos relevantes ser�o sempre os verticais e horizontais */
    private void addWeightNeighbors(char col, int row, int weight) {
        addWeightCell((char) (col - 1), row, weight);
        addWeightCell((char) (col + 1), row, weight);
        addWeightCell(col, row - 1, weight);
        addWeightCell(col, row + 1, weight);
    }

    private void calculateChances() {
        for (CellContainer c : cells.values()) {
            switch (c.state) {
            case WATER:
                addWeightNeighbors(c.column, c.row, 1);
                if (!sunkShips.contains(GridCell.AIRCRAFT_CARRIER))
                    c.weight += getContinuousAmmount(c.column, c.row, 5);
                if (!sunkShips.contains(GridCell.BATTLE_SHIP))
                    c.weight += getContinuousAmmount(c.column, c.row, 4);
                if (!sunkShips.contains(GridCell.DESTROYER))
                    c.weight += getContinuousAmmount(c.column, c.row, 3);
                if (!sunkShips.contains(GridCell.SUBMARINE))
                    c.weight += getContinuousAmmount(c.column, c.row, 3);
                if (!sunkShips.contains(GridCell.PATROL_BOAT))
                    c.weight += getContinuousAmmount(c.column, c.row, 2);
                break;
            case HIT:
                finishIt(c.column, c.row);
                break;
            case AIRCRAFT_CARRIER:
            case BATTLE_SHIP:
            case DESTROYER:
            case HIT_WATER:
            case PATROL_BOAT:
            case SUBMARINE:
            default:
                break;
            }
        }
    }

    private void finishIt(char col, int row) {
        CellContainer aux = cells.get((col * 10) + (row - 1));
        if (aux != null && aux.state.equals(GridCell.HIT))
            addWeightCell(col, row + 1, 60);
        else
            addWeightCell(col, row + 1, 30);

        aux = cells.get((col * 10) + (row + 1));
        if (aux != null && aux.state.equals(GridCell.HIT))
            addWeightCell(col, row - 1, 60);
        else
            addWeightCell(col, row - 1, 30);

        aux = cells.get(((col - 1) * 10) + row);
        if (aux != null && aux.state.equals(GridCell.HIT))
            addWeightCell((char) (col + 1), row, 60);
        else
            addWeightCell((char) (col + 1), row, 30);

        aux = cells.get(((col + 1) * 10) + row);
        if (aux != null && aux.state.equals(GridCell.HIT))
            addWeightCell((char) (col - 1), row, 60);
        else
            addWeightCell((char) (col - 1), row, 30);
    }

    private int getContinuousAmmount(char col, int row, int jumps) {
        int result = 0;
        for (int i = 0; i < 4; i++) {
            if (hasContinuity(col, row, i, jumps))
                result++;
        }
        return result;
    }

    private boolean hasContinuity(char col, int row, int direction, int jumps) {
        // Directions: 0 - up, 1 - right, 2 - down, 3 - left
        for (int i = 0; i < jumps; i++) {
            if ((col < 'A') || (col > 'J') || (row < 1) || (row > 10)
                    || (!cells.get(10 * col + row).state.equals(GridCell.WATER))) {
                return false;
            }
            switch (direction) {
            case 0:
                row--;
                break;
            case 1:
                col++;
                break;
            case 2:
                row++;
                break;
            case 3:
                col--;
                break;
            }
        }

        return true;
    }

    private CellContainer getHighestProb() {
        currentChoice = new CellContainer((char) 0, 0, Integer.MIN_VALUE);
        for (CellContainer o : cells.values()) {
            if (o.state.equals(GridCell.WATER) && (o.weight > currentChoice.weight)) {
                currentChoice = o;
            }
        }
        if (currentChoice == null)
            currentChoice = getRandom();
        return currentChoice;
    }

    private CellContainer getRandom() {
        int row = 0;
        int col = 0;

        do {
            final Random r = new Random();
            row = r.nextInt(10);
            col = r.nextInt(10);
        } while (!cells.get(((col + 'A') * 10) + row + 1).state.equals(GridCell.WATER));
        return cells.get(((col + 'A') * 10) + row + 1);
    }

    public JollyRoger(final String id) {
        super("Jolly Roger");
        this.cells = new HashMap<Integer, CellContainer>();
        this.sunkShips = new HashSet<GridCell>();

        CellContainer cur;
        for (int i = 0; i < 10; i++) {
            for (int j = 1; j < 11; j++) {
                cur = new CellContainer((char) ('A' + i), j);
                cells.put(((10 * ('A' + i)) + j), cur);
            }
        }
    }

    @Override
    public Torpedo act(final BattleGrid grid) {
        /*
         * Recupera o Estado Atual do tabuleiro guardando todos os detalhes das
         * c�lulas em uma cole��o de busca �gil.
         */
        for (int row = 1; row < 11; row++) {
            for (int col = 0; col < 10; col++) {
                int key = (10 * ('A' + col)) + row;
                CellContainer aux = cells.get(key);
                aux.weight = 0;
                aux.state = grid.getCell((char) (col + 'A'), row);

                switch (aux.state) {
                case AIRCRAFT_CARRIER:
                case BATTLE_SHIP:
                case DESTROYER:
                case PATROL_BOAT:
                case SUBMARINE:
                    sunkShips.add(aux.state);
                    break;
                case HIT:
                case HIT_WATER:
                case WATER:
                default:
                    break;
                }
            }
        }
        calculateChances();
        getHighestProb();
        return new Torpedo(this, currentChoice.column, currentChoice.row);
    }

    @Override
    public void reset() {
        sunkShips.clear();
    }

    private static class CellContainer implements Comparable<CellContainer> {
        private GridCell state;
        private int weight;
        private char column;
        private int row;

        public CellContainer(char column, int row, int weight) {
            this.column = column;
            this.row = row;
            this.weight = weight;
            this.state = GridCell.WATER;
        }

        public CellContainer(char column, int row) {
            this.column = column;
            this.row = row;
            this.weight = 0;
            this.state = GridCell.WATER;
        }

        @Override
        public String toString() {
            return "[" + ((char) column) + "" + row + "(" + weight + ")" + "]" + this.state;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + column;
            result = prime * result + row;
            result = prime * result + ((state == null) ? 0 : state.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            CellContainer other = (CellContainer) obj;
            if (column != other.column)
                return false;
            if (row != other.row)
                return false;
            if (state == null) {
                if (other.state != null)
                    return false;
            } else if (!state.equals(other.state))
                return false;
            return true;
        }

        @Override
        public int compareTo(CellContainer arg0) {
            if (this.equals(arg0))
                return 0;
            int result = this.weight - arg0.weight;
            if (result == 0)
                result = this.hashCode() - arg0.hashCode();
            return result;
        }

    }

}
