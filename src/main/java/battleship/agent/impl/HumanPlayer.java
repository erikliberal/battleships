package battleship.agent.impl;

import battleship.agent.BattleShipPlayer;
import battleship.environment.io.BattleGrid;
import battleship.environment.io.Torpedo;

public class HumanPlayer extends BattleShipPlayer {

	public HumanPlayer(String id) {
		super(id);
	}

	@Override
	public Torpedo act(BattleGrid grid) {
		int row=0;
		char col='A';
		return new Torpedo(this, col, row);
	}

	@Override
	public void reset() {
		
	}

}
