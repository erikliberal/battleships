/**
 * 
 */
package battleship.agent.impl;

import battleship.agent.BattleShipPlayer;
import battleship.environment.io.BattleGrid;
import battleship.environment.io.Torpedo;

public class SerialPlayer extends BattleShipPlayer {
    int i = 0;

    public SerialPlayer(final String id) {
        super(id);
    }

    @Override
    public Torpedo act(final BattleGrid grid) {
        final int row = this.i / 10;
        final int col = this.i % 10;
        this.i++;
        return new Torpedo(this, (char) (col + 'A'), row + 1);
    }

    @Override
    public void reset() {
        this.i = 0;
    }
}