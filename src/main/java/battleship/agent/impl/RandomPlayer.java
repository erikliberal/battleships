/**
 * 
 */
package battleship.agent.impl;

import java.util.Random;

import battleship.agent.BattleShipPlayer;
import battleship.environment.io.BattleGrid;
import battleship.environment.io.GridCell;
import battleship.environment.io.Torpedo;

public class RandomPlayer extends BattleShipPlayer {

    public RandomPlayer(final String id) {
        super(id);
    }

    @Override
    public Torpedo act(final BattleGrid grid) {
        int row = 0;
        int col = 0;

        do {
            final Random r = new Random();
            row = r.nextInt(10);
            col = r.nextInt(10);
        } while (grid.getCell((char) (col + 'A'), row + 1) != GridCell.WATER);
        return new Torpedo(this, (char) (col + 'A'), row + 1);
    }

    @Override
    public void reset() {
    }
}
