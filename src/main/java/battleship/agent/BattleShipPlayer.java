package battleship.agent;

import agent.Agent;
import battleship.environment.io.BattleGrid;
import battleship.environment.io.Torpedo;
import environment.io.Action;
import environment.io.Perception;

public abstract class BattleShipPlayer extends Agent {

    private int points = 0;

    public BattleShipPlayer(final String id) {
        super(id);
    }

    @Override
    public Action act(final Perception grid) {
        if (grid instanceof BattleGrid) {
            return this.act((BattleGrid) grid);
        } else {
            throw new IllegalArgumentException(
                    "Perception must be a BattleGrid");
        }
    }

    /**
     * Ação do jogador de batalha naval. Recebe um grid de batalha naval como
     * percepção e retorna um torpedo como ação a ser tomada.
     * 
     * @param grid
     *            {@link BattleGrid} posição atual do tabuleiro
     * @return {@link Torpedo} Torpedo a ser aplicado no tabuleiro do adversário
     */
    public abstract Torpedo act(BattleGrid grid);

    /**
     * Método chamado no início de cada partida
     */
    public abstract void reset();

    /**
     * Obtém a quantidade de pontos (partidas vencidas) pelo jogador.
     * 
     * @return a quantidade de pontos (partidas vencidas) pelo jogador.
     */
    public int getPoints() {
        return this.points;
    }

    /**
     * Incrementa a quantidade de pontos do jogador.
     */
    public void addPoint() {
        this.points++;
    }
}
