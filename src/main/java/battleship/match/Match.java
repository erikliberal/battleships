package battleship.match;

import static battleship.environment.BattleField.randomBattleField;
import battleship.agent.BattleShipPlayer;
import battleship.environment.BattleField;

public class Match {
    private final BattleShipPlayer player1;
    private final BattleShipPlayer player2;
    private final BattleField battleField;
    private final BattleField battleField2;

    public Match(BattleShipPlayer player1, BattleShipPlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.battleField = randomBattleField();
        this.battleField2 = randomBattleField();
    }

    public void start() {
        this.player1.reset();
        this.player2.reset();
        this.battleField.addAgent(player1);
        this.battleField2.addAgent(player2);
    }

    public void playTurn() {
        battleField.playTurn();
        battleField2.playTurn();
    }

    public BattleField getBattleField() {
        return battleField;
    }

    public BattleField getBattleField2() {
        return battleField2;
    }

    public boolean isFinished() {
        return battleField.getShipsSunk() >= 5 || battleField2.getShipsSunk() >= 5;
    }

    public MatchResult getResult() {
        BattleShipPlayer winner, loser;

        int shipsSunkBf = battleField.getShipsSunk();
        int shipsSunkBf2 = battleField2.getShipsSunk();

        if (shipsSunkBf > shipsSunkBf2) {
            winner = player1;
            loser = player2;
        } else if (shipsSunkBf > shipsSunkBf2) {
            winner = player2;
            loser = player1;
        } else {
            winner = null;
            loser = null;
        }

        return new MatchResult(winner, loser);
    }

    public static class MatchResult {
        private BattleShipPlayer winner, loser;

        private MatchResult(BattleShipPlayer winner, BattleShipPlayer loser) {
            this.winner = winner;
            this.loser = loser;
        }

        public BattleShipPlayer getWinner() {
            return winner;
        }

        public BattleShipPlayer getLoser() {
            return loser;
        }

        public boolean isTie() {
            return winner == null && loser == null;
        }
    }

}
