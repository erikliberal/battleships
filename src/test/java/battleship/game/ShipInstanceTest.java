package battleship.game;

import static java.text.MessageFormat.format;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ShipInstanceTest {

    @Test
    public void shootingShipInstanceTest() {
        for (Ship ship : Ship.values()) {
            ShipInstance shipInstance = new ShipInstance(ship);
            int shipSize = ship.getSize();
            for (int i = 0; i < shipSize - 1; i++) {
                validateRegularHit(shipInstance.hit());
            }
            validateSinkingHit(shipInstance.hit());
        }
        validateMissHit(Shot.MISS);
    }

    private void validateMissHit(Shot shot) {
        boolean hit = shot.isHit();
        assertFalse(format("Should not have been a hit: {0}", hit), hit);
        Ship ship = shot.getShip();
        assertNull(format("Should have been null: {0}", ship), ship);
    }

    private void validateRegularHit(Shot shot) {
        boolean hit = shot.isHit();
        assertTrue(format("Should have been a hit: {0}", hit), hit);
        Ship ship = shot.getShip();
        assertNull(format("Should have been null: {0}", ship), ship);
    }

    private void validateSinkingHit(Shot shot) {
        boolean hit = shot.isHit();
        assertTrue(format("Should have been a hit: {0}", hit), hit);
        Ship ship = shot.getShip();
        assertNotNull(format("Should have been a ship: {0}", ship), ship);
    }
}
