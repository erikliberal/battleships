package battleship.game;

import static battleship.game.Direction.HORIZONTAL;
import static battleship.game.Direction.VERTICAL;
import static battleship.game.Ship.AIRCRAFT_CARRIER;
import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PositionTest {
    private static final String SHOULD_NOT_ALLOW_PLACEMENT = "{0} {1} should not allow placement";
    private static final String SHOULD_ALLOW_PLACEMENT = "{0} {1} should allow placement";
    private static final String SHIP_NOT_FOUND = "Ship should have been found in position {0}";

    @Test
    public void newPositionTest() {
        assertEquals(new Position(1, 1).toString(), new Position('A', 1).toString());
        assertEquals(new Position(5, 5).toString(), new Position('E', 5).toString());
        assertEquals(new Position(10, 10).toString(), new Position('J', 10).toString());
    }

    @Test
    public void hasPlacedShipTest() {
        for (Ship ship : Ship.values()) {
            for (int i = 0; i < 10; i++) {
                int gridLimit = 11 - ship.getSize();
                for (int j = 0; j < gridLimit; j++) {
                    verifyShipPositioning(new Position((char) ('A' + j), 1 + i), ship, HORIZONTAL);
                    verifyShipPositioning(new Position((char) ('A' + i), 1 + j), ship, VERTICAL);
                }
            }
        }
    }

    private void assertPlacementAllowed(Grid playerGrid, char col, int row, Ship ship, Direction direction) {
        assertPlacementAllowed(playerGrid, new Position(col, row), ship, direction);
    }

    private void assertPlacementAllowed(Grid playerGrid, Position position, Ship ship, Direction direction) {
        String msg = format(SHOULD_ALLOW_PLACEMENT, position, direction);
        assertTrue(msg, playerGrid.isPlacementAllowed(ship, position, direction));
    }

    private void assertPlacementNotAllowed(Grid playerGrid, char col, int row, Ship ship, Direction direction) {
        assertPlacementNotAllowed(playerGrid, new Position(col, row), ship, direction);
    }

    private void assertPlacementNotAllowed(Grid playerGrid, Position position, Ship ship, Direction direction) {
        String msg = format(SHOULD_NOT_ALLOW_PLACEMENT, position, direction);
        assertFalse(msg, playerGrid.isPlacementAllowed(ship, position, direction));
    }

    @Test
    public void isPlacementAllowedTest() {
        Grid playerGrid = new Grid(10);
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                assertPlacementAllowed(playerGrid, (char) (j + 'A'), i + 1, AIRCRAFT_CARRIER, HORIZONTAL);
                assertPlacementAllowed(playerGrid, (char) (i + 'A'), j + 1, AIRCRAFT_CARRIER, VERTICAL);
                assertPlacementNotAllowed(playerGrid, (char) (i + 'A'), j + 7, AIRCRAFT_CARRIER, VERTICAL);
                assertPlacementNotAllowed(playerGrid, (char) (j + 'G'), i + 1, AIRCRAFT_CARRIER, HORIZONTAL);

            }
        }
        playerGrid.placeShip(AIRCRAFT_CARRIER, new Position('E', 5), VERTICAL);
        for (int j = 0; j < 6; j++) {
            for (int i = 0; i < 4; i++) {
                assertPlacementAllowed(playerGrid, (char) ('A' + j), i + 1, AIRCRAFT_CARRIER, HORIZONTAL);
            }
            assertPlacementAllowed(playerGrid, (char) ('A' + j), 10, AIRCRAFT_CARRIER, HORIZONTAL);
        }
        for (int j = 0; j < 5; j++) {
            for (int i = 0; i < 5; i++) {
                assertPlacementNotAllowed(playerGrid, (char) ('A' + j), i + 5, AIRCRAFT_CARRIER, HORIZONTAL);
            }
        }
        for (int i = 0; i < 6; i++) {
            assertPlacementNotAllowed(playerGrid, 'E', i + 1, AIRCRAFT_CARRIER, VERTICAL);
            assertPlacementAllowed(playerGrid, 'D', i + 1, AIRCRAFT_CARRIER, VERTICAL);
            assertPlacementAllowed(playerGrid, 'F', i + 1, AIRCRAFT_CARRIER, VERTICAL);
        }
    }

    private void verifyShipPositioning(Position position, Ship ship, Direction direction) {
        Grid playerGrid = new Grid(10);
        playerGrid.placeShip(ship, position, direction);
        if (HORIZONTAL.equals(direction)) {
            verifyPositioningHorizontal(playerGrid, position, ship);
        } else if (VERTICAL.equals(direction)) {
            verifyPositioningVertical(playerGrid, position, ship);
        } else {
            assertFalse("Non existant direction", Boolean.TRUE);
        }
    }

    private void verifyPositioningHorizontal(Grid playerGrid, Position position, Ship ship) {
        for (int i = 0; i < ship.getSize(); i++) {
            final Position newPosition = new Position(position.getColumn() + i, position.getRow());
            final boolean hasPlacedShip = playerGrid.hasPlacedShip(newPosition);
            assertTrue(format(SHIP_NOT_FOUND, newPosition), hasPlacedShip);
        }
    }

    private void verifyPositioningVertical(Grid playerGrid, Position position, Ship ship) {
        for (int i = 0; i < ship.getSize(); i++) {
            final Position newPosition = new Position(position.getColumn(), position.getRow() + i);
            final boolean hasPlacedShip = playerGrid.hasPlacedShip(newPosition);
            assertTrue(format(SHIP_NOT_FOUND, newPosition), hasPlacedShip);
        }
    }

}
