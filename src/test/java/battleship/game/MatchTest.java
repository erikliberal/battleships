package battleship.game;

import static battleship.game.Direction.HORIZONTAL;
import static battleship.game.Ship.AIRCRAFT_CARRIER;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class MatchTest {

    private static final class Player extends battleship.game.Player {
        public Player(String id) {
            super(id);
        }
    }

    @Test
    public void placementTest() {
        Player player1 = new Player("teste1");
        Player player2 = new Player("teste2");
        Match match = new Match();
        player1.joinMatch(match);
        player2.joinMatch(match);
        assertEquals("GAME PHASE SHOULD BE PLACEMENT", match.getCurrentPhase(), Match.MatchPhase.PLACEMENT);
        assertEquals("FIRST PLAYER 1", match.getCurrentPlayer(), player1);
        assertFalse("Game just started should not be ended", match.isGameEnded());
        assertTrue("Should be allowed to place shipts during placement phase",
                match.isPlacementAllowed(player1, AIRCRAFT_CARRIER, new Position('A', 1), HORIZONTAL));
        match.placeShip(player1, AIRCRAFT_CARRIER, new Position('A', 1), HORIZONTAL);
        assertNull("Should not be allowed to shoot during placement phase", match.shoot(player1, new Position('A', 5)));
        assertTrue("Should be allowed to place shipts during placement phase",
                match.isPlacementAllowed(player2, AIRCRAFT_CARRIER, new Position('A', 1), HORIZONTAL));
        match.placeShip(player2, AIRCRAFT_CARRIER, new Position('A', 1), HORIZONTAL);
        assertNull("Should not be allowed to shoot during placement phase", match.shoot(player2, new Position('A', 5)));
    }

    @Test
    public void gameTest() {
        Player player1 = new Player("teste1");
        Player player2 = new Player("teste2");
        final Map<Ship, Integer> maxShips = new HashMap<Ship, Integer>();

        maxShips.put(AIRCRAFT_CARRIER, 1);

        Match match = new Match(maxShips.entrySet(), 10);
        player1.joinMatch(match);
        player2.joinMatch(match);

        match.placeShip(player1, AIRCRAFT_CARRIER, new Position('A', 1), HORIZONTAL);
        match.placeShip(player2, AIRCRAFT_CARRIER, new Position('A', 1), HORIZONTAL);

        assertEquals("GAME PHASE SHOULD BE GAME", match.getCurrentPhase(), Match.MatchPhase.GAME);

        assertEquals("SHOULD BE PLAYER 1", match.getCurrentPlayer(), player1);

        Shot shot = match.shoot(player2, new Position('A', 1));
        assertNull("Shot should be null", shot);

        shot = match.shoot(player1, new Position('A', 2));
        assertEquals("Shot should be miss", shot, Shot.MISS);

        assertEquals("SHOULD BE PLAYER 2", match.getCurrentPlayer(), player2);

        shot = match.shoot(player1, new Position('A', 1));
        assertNull("Shot should be null", shot);

        shot = match.shoot(player2, new Position('A', 1));
        assertTrue("Shot should be a hit", shot.isHit());
        shot = match.shoot(player1, new Position('A', 1));
        assertTrue("Shot should be a hit", shot.isHit());

        shot = match.shoot(player2, new Position('B', 1));
        assertTrue("Shot should be a hit", shot.isHit());
        shot = match.shoot(player1, new Position('B', 1));
        assertTrue("Shot should be a hit", shot.isHit());

        shot = match.shoot(player2, new Position('C', 1));
        assertTrue("Shot should be a hit", shot.isHit());
        shot = match.shoot(player1, new Position('C', 1));
        assertTrue("Shot should be a hit", shot.isHit());

        shot = match.shoot(player2, new Position('D', 1));
        assertTrue("Shot should be a hit", shot.isHit());
        shot = match.shoot(player1, new Position('D', 1));
        assertTrue("Shot should be a hit", shot.isHit());

        shot = match.shoot(player2, new Position('E', 1));
        assertTrue("Shot should be a hit", shot.isHit());

        assertTrue("Game should have ended", match.isGameEnded());
        assertEquals("Winner should be player 2", match.getWinner(), player2);

        shot = match.shoot(player1, new Position('E', 1));
        assertNull("Shot should be null", shot);

    }

}
