package battleship.game;

import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class ScoreTest {

    @Test
    public void validateShipAmmountAndCounting() {
        final Map<Ship, Integer> maxShips = new HashMap<Ship, Integer>();
        for (Ship ship : Ship.values()) {
            maxShips.put(ship, Integer.valueOf(1));
        }
        Score score = new Score(maxShips.entrySet());
        for (Ship ship : Ship.values()) {
            validateShipQuantity(ship, 1, score);
        }
        for (Ship ship : Ship.values()) {
            for (int i = 0; i < score.getAmmount(ship); i++) {
                validateShipSunkQuantity(ship, i, score);
                score.sink(ship);
                validateShipSunkQuantity(ship, i + 1, score);
            }
            IllegalStateException e = null;
            try {
                score.sink(ship);
            } catch (IllegalStateException e1) {
                e = e1;
            }
            assertNotNull("Thrown exception was expected", e);
        }
    }

    private void validateShipSunkQuantity(Ship ship, int expectedAmmount, Score score) {
        int ammountSunk = score.getAmmountSunk(ship);
        assertEquals(
                format("Should have ''{0}'' ammount of type {1} Sunk. Found {2}", expectedAmmount, ship, ammountSunk),
                ammountSunk, expectedAmmount);
    }

    private void validateShipQuantity(Ship ship, int expectedAmmount, Score score) {
        int foundAmmount = score.getAmmount(ship);
        assertEquals(format("Should have ''{0}'' ammount of type {1}. Found {2}", expectedAmmount, ship, foundAmmount),
                foundAmmount, expectedAmmount);
    }

}
