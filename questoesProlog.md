Questão 1

passaro('Marcelo').
peixe('Silvio').
minhoca('Ana').
gato('Chuck Norris').
pessoa('Eu').
dono('Eu', 'Chuck Norris').
gosta(X,Y) :- (passaro(X) , minhoca(Y)) ; (gato(X) , peixe(Y)) ; (gato(X) , passaro(Y)) ; (dono(X,Y) ; dono(Y,X)).
come(X,Y) :- gato(X) , gosta(X,Y) , not(pessoa(Y)).
amigo(X,Y) :- gosta(X,Y) , gosta(Y,X).

Questão 2

mulher('Maria').
mulher('Ana').
homem('Marcos').
homem('Silvio').
homem('Fabiano').
bonito('Maria').
bonito('Marcos').
bonito('Fabiano').
rico('Marcos').
rico('Ana').
forte('Ana').
forte('Fabiano').
forte('Silvio').
amavel('Silvio').

gosta(X,Y) :- (homem(X) , mulher(Y) , bonito(Y)).
gosta('Ana',X) :- (homem(X) , gosta(X,'Ana')).
gosta('Maria', X) :- homem(X) , gosta(X, 'Maria'), (rico(X), amavel(X)) ; (bonito(X) , forte(X)).
feliz(X) :- (homem(X) , rico(X)) ; (homem(X), mulher(Y), gosta(X,Y) , gosta(Y,X) ) ; (homem(Y), mulher(X), gosta(X,Y) , gosta(Y,X) ) .

Questão 3

aluno(joao, calculo).
aluno(maria, calculo).
aluno(joel, programacao).
aluno(joel, estrutura).
frequenta(joao, puc).
frequenta(maria, puc).
frequenta(joel, ufrj).
professor(carlos, calculo).
professor(ana_paula, estrutura).
professor(pedro, programacao).
funcionario(pedro, ufrj).
funcionario(ana_paula, puc).
funcionario(carlos, puc).

ensina(X,Y) :- professor(X,Z) , aluno(Y,Z) , frequenta(Y,M) , funcionario(X,M).
universidade(X,Y) :- frequenta(X,Y) ; funcionario(X,Y).

Questão 4

nota(joao,5.0).
nota(maria,6.0).
nota(joana,8.0).
nota(mariana,9.0).
nota(cleuza,8.5).
nota(jose,6.5).
nota(jaoquim,4.5).
nota(mara,4.0).
nota(mary,10.0).

aprovado(X) :- nota(X,Y) , Y>=7.0 .
recuperacao(X) :- nota(X,Y) , Y >=5.0 , Y < 7.0 .
reprovado(X) :- nota(X,Y) , Y < 5.0 .

Questão 5

filme('Amnésia').
filme('Babel').
filme('Capote').
filme('Casablanca').
filme('Matrix').
filme('Rebecca').
filme('Shrek').
filme('Sinais').
filme('Spartacus').
filme('Superman').
filme('Titanic').
filme('Tubarão').
filme('Volver').

genero('Amnésia','Suspense').
genero('Babel','Drama').
genero('Capote','Drama').
genero('Casablanca','Romance').
genero('Matrix','Ficção').
genero('Rebecca','Suspense').
genero('Shrek','Aventura').
genero('Sinais','Ficção').
genero('Spartacus','Ação').
genero('Superman','Aventura').
genero('Titanic','Romance').
genero('Tubarão','Suspense').
genero('Volver','Drama').

diretor('Amnésia','Nolan').
diretor('Babel','Inarritu').
diretor('Capote','Miller').
diretor('Casablanca','Curtiz').
diretor('Matrix','Wachowsk').
diretor('Rebecca','Hitchcock').
diretor('Shrek','Adamson').
diretor('Sinais','Shymalan').
diretor('Spartacus','Kubrik').
diretor('Superman','Donner').
diretor('Titanic','Cameron').
diretor('Tubarão','Spielberg').
diretor('Volver','Almodóvar').

ano('Amnésia',2000).
ano('Babel',2006).
ano('Capote',2005).
ano('Casablanca',1942).
ano('Matrix',1999).
ano('Rebecca',1940).
ano('Shrek',2001).
ano('Sinais',2002).
ano('Spartacus',1960).
ano('Superman',1978).
ano('Titanic',1997).
ano('Tubarão',1975).
ano('Volver',2006).

duracao('Amnésia',113).
duracao('Babel',142).
duracao('Capote',98).
duracao('Casablanca',102).
duracao('Matrix',136).
duracao('Rebecca',130).
duracao('Shrek',90).
duracao('Sinais',106).
duracao('Spartacus',184).
duracao('Superman',143).
duracao('Titanic',194).
duracao('Tubarão',124).
duracao('Volver',121).

?- diretor('Titanic', X).
?- genero(X, 'Suspense').
?- diretor(X, 'Donner').
?- ano('Sinais', X).
?- duracao(X, Y) , Y < 100.
?- ano(X,Y) , Y >= 2000 , Y <= 2005
